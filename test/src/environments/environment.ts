// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDN2LxUW9zzUrCMr71OCQufzE8nu-_N9YY",
    authDomain: "test-e710e.firebaseapp.com",
    projectId: "test-e710e",
    storageBucket: "test-e710e.appspot.com",
    messagingSenderId: "811068786011",
    appId: "1:811068786011:web:b06882d0c6db59bcf0c67e"
  }
};

/*
 * **For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
